let a, b, c
console.log(a, b, c)
a = a + 2
b = b - 5
c = c + "string"
console.log(a, b, c)
a = 34
b = 20
c = "new"
a = a + 2
b = b - 5
c = c + " string"
console.log(a, b, c)
//
let hours = 3
let minutes = hours * 60
let seconds = hours * 3600
let miliseconds = seconds * 1000

console.log(hours, minutes, seconds, miliseconds)

//
let h = 12,
    m = 35,
    s = 16


console.log(`${h}:${m}/${s}`)